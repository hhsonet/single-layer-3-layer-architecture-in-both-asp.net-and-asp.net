﻿using System.Collections.Generic;
using System.Linq;
using DemoMvc3Layer.DataModel;

namespace DemoMvc3Layer.DAL
{
    public class StudentDataAccessLayer
    {
        private readonly DataContext _db;
        public StudentDataAccessLayer()
        {
            _db = new DataContext();
        }

        public List<Student> GetAllStudent()
        {
            return _db.Students.ToList();
        }
    }
}
