using DemoMvc3Layer.DataModel;

namespace DemoMvc3Layer.DAL.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DemoMvc3Layer.DAL.DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(DemoMvc3Layer.DAL.DataContext context)
        {
            context.Students.AddOrUpdate(
                  p => p.FirstName,
                  new Student { FirstName = "Abdur", LastName = "Rahim"},
                  new Student { FirstName = "Hasan", LastName = "Abdullah"}
                );
        }
    }
}
