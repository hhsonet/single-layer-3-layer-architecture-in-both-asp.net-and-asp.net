﻿using System.Collections.Generic;
using System.Web.Mvc;
using DemoMvc3Layer.BAL;
using DemoMvc3Layer.DataModel;

namespace DemoMvc3Layer.Presentation.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            StudentService service = new StudentService();
            List<Student> students = service.GetAllStudents();
            return View(students);
        }
    }
}