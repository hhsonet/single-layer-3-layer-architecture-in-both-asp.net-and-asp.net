using DemoMvcApi.Models;

namespace DemoMvcApi.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DemoMvcApi.Database.DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DemoMvcApi.Database.DataContext context)
        {
            context.Students.AddOrUpdate(
                 p => p.FirstName,
                 new Student { FirstName = "Abdur", LastName = "Rahim" },
                 new Student { FirstName = "Hasan", LastName = "Abdullah" }
               );
        }
    }
}
