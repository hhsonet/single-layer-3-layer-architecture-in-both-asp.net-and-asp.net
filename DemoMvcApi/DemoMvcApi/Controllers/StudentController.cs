﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using DemoMvcApi.Database;
using DemoMvcApi.Models;

namespace DemoMvcApi.Controllers
{
    public class StudentController : ApiController
    {
        private readonly DataContext _context;

        public StudentController()
        {
            _context = new DataContext();
        }


        // GET: api/Student
        public IHttpActionResult Get()
        {
            List<Student> students = _context.Students.ToList();
            return Ok(students);
        }

        // POST: api/Student
        public IHttpActionResult Post(Student data)
        {
            try
            {
                _context.Students.Add(data);
                _context.SaveChanges();
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
