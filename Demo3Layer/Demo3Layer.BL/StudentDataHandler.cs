﻿using System.Collections.Generic;
using Demo3Layer.DAL;
using Demo3Layer.Model;

namespace Demo3Layer.BL
{
    public class StudentDataHandler
    {
        public List<Student> GetAllStudent()
        {
            StudentData data = new StudentData();
            return data.GetAll();
        }
    }
}
